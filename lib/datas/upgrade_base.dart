///@author:create by BZY
///Date: 2022/12/26 11:02
///Description: 升级 model 基础数据类

abstract class UpgradeBase {
  ///更新弹窗标题
  String getTitle();

  ///更新内容
  String getInfo();

  ///是否强制更新
  bool getForce();

  ///版本名称
  String getVersionName();

  ///版本号
  int getVersionCode();

  ///更新包大小 单位M
  int getFileSize();
}

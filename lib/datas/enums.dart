enum ShapeData { circle, round, standard, square }

enum SizeData { large, middle, small }

enum AlignData{
  topLeft,
  topRight,
  topCenter,
  center,
  centerRight,
  centerLeft,
  bottomLeft,
  bottomRight,
  bottomCenter
}

///更新下载状态
enum DownloadStatus{
  undefined,
  running,
  pause,
  fail,
  done
}

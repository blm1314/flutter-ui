import 'package:common_ui/datas/enums.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///普通的button组件,不带有任何状态
class CommonButton extends StatelessWidget {
  final VoidCallback? onPress;
  final String text;
  final double? wide;
  final double? height;
  final TextStyle? textStyle;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final Color? bgColor;
  final AlignmentGeometry? aligment;
  final double? elevation;
  final ShapeData shape;
  final double? round;
  final BorderSide? borderSide;
  final Gradient? gradient;
  final EdgeInsetsGeometry? btnPadding;
  final int? tapInterval;

  ///当前按钮是否可用
  final bool? enable;

  var lastTapTime;

  CommonButton(this.text,
      {Key? key,
      this.onPress,
      this.wide,
      this.height = 35,
      this.textStyle,
      this.margin,
      this.padding,
      this.bgColor,
      this.elevation,
      this.round,
      this.enable,
      this.borderSide,
      this.aligment,
      this.gradient,
      this.shape = ShapeData.round,
      this.btnPadding,
      this.tapInterval})
      : super(key: key);

  ///如果按钮不可用,则直接设置为灰色
  ///如果可用根据设置的背景色进行设置
  Color get getBgColor {
    if (enable ?? true) {
      return bgColor ?? Colors.blue;
    } else {
      return Colors.grey;
    }
  }

  @override
  Widget build(BuildContext context) {
    final setInterval = tapInterval ?? 500;
    return Container(
      margin: margin,
      padding: padding,
      width: wide,
      alignment: aligment,
      height: height,
      decoration: BoxDecoration(
          gradient: gradient,
          borderRadius: BorderRadius.circular(
              shape == ShapeData.circle ? (height ?? 20) / 2.0 : round ?? 5)),
      child: ElevatedButton(
        onPressed: () {
          if ((enable ?? true) &&
              (setInterval <= 0 ||lastTapTime==null||
                  DateTime.now().difference(lastTapTime) >
                      Duration(milliseconds: setInterval))) {
            onPress?.call();
          }
          lastTapTime = DateTime.now();
        },
        style: ButtonStyle(
            shape: MaterialStateProperty.all(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(shape == ShapeData.circle
                    ? (height ?? 20) / 2.0
                    : round ?? 5))),
            backgroundColor: MaterialStateProperty.all(getBgColor),
            shadowColor: MaterialStateProperty.all(Colors.transparent),
            side: MaterialStateProperty.all(borderSide),
            padding: MaterialStateProperty.all(btnPadding),
            elevation: MaterialStateProperty.all(elevation)),
        child: Text(
          text,
          style: textStyle ?? TextStyle(color: Colors.white, fontSize: 16),
        ),
      ),
    );
  }
}

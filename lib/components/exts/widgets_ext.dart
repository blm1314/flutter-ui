import 'package:common_ui/common_ui.dart';
import 'package:common_ui/components/common_avatar.dart';
import 'package:common_ui/components/common_button.dart';
import 'package:common_ui/components/common_text.dart';
import 'package:common_ui/components/common_edit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

extension WidgetAlignment on Widget {
  ///设置alignment 值
  Widget setAlign(AlignmentGeometry align) {
    return UnconstrainedBox(
      alignment: align,
      child: this,
    );
  }

  ///设置组件点击事件
  Widget onClick(VoidCallback onClick,{Key? key}) {
    return InkWell(
      key: key,
      onTap: () => onClick.call(),
      child: this,
    );
  }
}

///设置margin 值
extension WidgetMargin on Widget {
  Widget setMargin(EdgeInsetsGeometry margin) {
    return Container(
      margin: margin,
      child: this,
    );
  }
}

///设置padding值
extension WidgetPadding on Widget {
  Widget setPadding(EdgeInsetsGeometry padding) {
    return Container(
      padding: padding,
      child: this,
    );
  }
}

///搜索框
Widget searchBar(String hint, Function(String) search, {Color? barBgColor}) {
  return Container(
    padding: const EdgeInsets.only(left: 12),
    width: double.infinity,
    height: 40,
    alignment: Alignment.center,
    decoration: BoxDecoration(
        color: barBgColor ?? Colors.white,
        borderRadius: BorderRadius.circular(20)),
    child: TextField(
      style: TextStyle(color: Colors.black87, fontSize: 16),
      onChanged: (value) => search.call(value),
      decoration: InputDecoration(
          contentPadding: const EdgeInsets.only(left: -8),
          isCollapsed: true,
          icon: Icon(
            CupertinoIcons.search,
            size: 24,
            color: Colors.grey[400],
          ),
          hintText: hint,
          border: InputBorder.none),
    ),
  );
}

///底部分割线
Border line({Color? lineColor, double? height}) {
  return Border(
      bottom: BorderSide(
          color: lineColor ?? Color(0xFFE0E0E0), width: height ?? 1));
}

///铺满宽度的圆角按钮
Widget getCommonButton(String text,
    {required VoidCallback onPress, EdgeInsetsGeometry? margin}) {
  return CommonButton(
    text,
    onPress: onPress,
    margin: margin ?? const EdgeInsets.all(16),
    wide: double.infinity,
    height: 45,
    shape: ShapeData.circle,
  );
}

///单个编辑框
Widget getSingleEdit({String? content,
  String? hint,
  int? maxLines,
  int? minLines,
  Color? lineColor,
  double? lineHeight,
  Function(String)? onChange,
  EdgeInsetsGeometry? margin}) {
  return SingleEdit(
    color: Colors.white,
    minLines: minLines,
    maxLines: maxLines,
    onChanged: onChange,
    controller:
    (content ?? '').isEmpty ? null : TextEditingController(text: content),
    decoration: InputDecoration(
      hintText: hint,
      border: lineColor == null
          ? InputBorder.none
          : OutlineInputBorder(
          borderSide:
          BorderSide(color: Color(0xFFE0E0E0), width: lineHeight ?? 1)),
      contentPadding: const EdgeInsets.all(16),
    ),
  );
}

///圆形头像组件
Widget getAvatar(String? url, double size,
    {EdgeInsetsGeometry? margin, String? placeHolder}) {
  ImageProvider? image;
  if ((url ?? '').isEmpty) {
    image = AssetImage(placeHolder ?? '');
  } else {
    image = NetworkImage(url!);
  }
  return Avatar(
    backgroundImage: image,
    size: size,
    margin: margin,
  );
}

///向右的箭头组件
Widget getArrowRightIcon() {
  return Icon(
    Icons.arrow_forward_ios_outlined,
    color: Colors.grey[400],
    size: 20,
  );
}

///左边标题,右边内容不带箭头的组件,带有下划线
Widget getCommonTitleContent(
    {String? title, String? content, Color? lineColor, double? lineHeight}) {
  return TitleContentText(
    title: title,
    content: content,
    padding: const EdgeInsets.fromLTRB(16, 12, 16, 12),
    titleStyle: TextStyle(color: Colors.black87, fontSize: 16),
    contentStyle: TextStyle(color: Colors.black87, fontSize: 16),
    border: Border(
        bottom: BorderSide(
            color: lineColor ?? Color(0xFFE0E0E0), width: lineHeight ?? 1)),
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    alignment: Alignment.centerRight,
  );
}

///左边标题,右边内容带箭头的组件,带有下划线
Widget getArrowTitleContent(
    {String? title, String? content, int? maxLines, Function()? onClick}) {
  return TitleContentIcon(
    title: title,
    content: content,
    padding: const EdgeInsets.fromLTRB(16, 12, 6, 12),
    titleStyle: TextStyle(color: Colors.black87, fontSize: 16),
    contentStyle: TextStyle(color: Colors.black87, fontSize: 16),
    onClick: onClick,
    backColor: Colors.white,
    maxLines: maxLines,
    border: Border(bottom: BorderSide(color: Color(0xFFE0E0E0))),
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    alignment: Alignment.centerRight,
    contentIcon: getArrowRightIcon(),
  );
}

///提示信息组件
Widget notifyView(String notify) {
  return SingleText(
    notify,
    height: 35,
    alignment: Alignment.centerLeft,
    textStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
    margin: const EdgeInsets.only(left: 16),
    bgColor: Color.fromARGB(255, 241, 244, 249),
  );
}

///toolbar 右侧的操作按钮
Widget actionButton(String text, VoidCallback onPress) {
  return TextButton(
      onPressed: onPress,
      child: Text(
        text,
        style: TextStyle(color: Colors.white, fontSize: 14),
      ));
}

///有外框的textButton
Widget textLineButton(VoidCallback? onPressed, String text,
    {Key?key, EdgeInsetsGeometry? padding, Color? lineColor, TextStyle? style, double? radius}) {
  return TextButton(
      key: key,
      onPressed: onPressed,
      style: ButtonStyle(
          minimumSize: MaterialStateProperty.all(Size.zero),
          padding: MaterialStateProperty.all(
              padding ?? EdgeInsets.fromLTRB(8, 2, 8, 2)),
          backgroundColor: MaterialStateProperty.all(Colors.white),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              side: BorderSide(color: lineColor ?? Colors.blue),
              borderRadius: BorderRadius.circular(radius ?? 6)))),
      child: Text(
        text,
        style: style ?? TextStyle(color: Colors.blue, fontSize: 14),
      ));
}

///有背景色的textButton
Widget textBgButton(VoidCallback? onPressed, String text,
    {Key?key, EdgeInsetsGeometry? padding, Color? bgColor, TextStyle? style, double? radius}) {
  return TextButton(
      key: key,
      onPressed: onPressed,
      style: ButtonStyle(
          minimumSize: MaterialStateProperty.all(Size.zero),
          padding: MaterialStateProperty.all(padding),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          backgroundColor: MaterialStateProperty.all(bgColor ?? Colors.blue),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(radius ?? 6)))),
      child: Text(
        text,
        style: style ?? TextStyle(color: Colors.white, fontSize: 14),
      ));
}

///大小为16的黑色字体
Widget commonText(String text) {
  return Text(text, style: TextStyle(color: Colors.black87, fontSize: 16),);
}

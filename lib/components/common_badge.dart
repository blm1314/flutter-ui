import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///计数角标
class Badge extends StatelessWidget {
  ///只需要指定高度,宽度使用自适应
  final double height;
  final int count;
  final double? textSize;

  Badge({required this.height, required this.count, this.textSize = 10});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: height,
      width: count > 9 ? null : height,
      constraints: BoxConstraints(minWidth: height, maxWidth: height * 1.5),
      decoration: BoxDecoration(
          color: Colors.red, borderRadius: BorderRadius.circular(height / 2)),
      child: Text(
        count > 99 ? "99+" : count.toString(),
        style: TextStyle(color: Colors.white, fontSize: textSize),
      ),
    );
  }
}

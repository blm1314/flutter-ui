import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///@author:create by BZY
///Date: 2021/8/23 17:28
///Description:带有搜索按钮的搜索栏

class SearchBar extends StatelessWidget {
  final String hint;
  final Color? bgColor;
  final Color? searchColor;
  final Color? buttonBgColor;
  final Function(String)? onChange;
  final VoidCallback? onPress;
  final FocusNode? focusNode;
  final bool? autofocus;
  final TextInputType? inputType;

  SearchBar({Key? key,
    required this.hint,
    this.bgColor,
    this.buttonBgColor,
    this.onChange,
    this.onPress,
    this.focusNode,
    this.searchColor, this.autofocus, this.inputType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: 50,
      color: bgColor ?? Colors.grey[200],
      child: Row(
        children: [
          Expanded(
              child: Container(
                margin: const EdgeInsets.only(left: 16, right: 12),
                height: 35,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: searchColor ?? Colors.white,
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 12),
                      child: const Icon(
                        Icons.search_rounded,
                        color: Colors.grey,
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        onChanged: (value) {
                          onChange?.call(value);
                        },
                        keyboardType: inputType,
                        focusNode: focusNode,
                        autofocus: autofocus ?? false,
                        textInputAction: TextInputAction.search,
                        onSubmitted: (value) {
                          onPress?.call();
                        },
                        decoration: InputDecoration(
                            hintText: hint,
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors
                                    .transparent)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors
                                    .transparent)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors
                                    .transparent)),
                            contentPadding: EdgeInsets.zero),
                        style: TextStyle(color: Colors.black87, fontSize: 16),
                      ),
                    )
                  ],
                ),
              )),
          Container(
            margin: EdgeInsets.only(right: 6),
            child: TextButton(
                onPressed: () {
                  onPress?.call();
                },
                style: ButtonStyle(
                    padding: MaterialStateProperty.all(
                        EdgeInsets.fromLTRB(8, 4, 8, 4)),
                    minimumSize: MaterialStateProperty.all(Size.zero),
                    backgroundColor: MaterialStateProperty.all(
                        buttonBgColor ?? Colors.blue)),
                child: Text(
                  '搜索',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                )),
          )
        ],
      ),
    );
  }
}

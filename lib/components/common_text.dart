import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///单个text组件
class SingleText extends StatelessWidget {
  final String text;
  final TextStyle? textStyle;
  final double? wide;
  final double? height;
  final int? maxLines;
  final Color? bgColor;
  final AlignmentGeometry? alignment;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  final BoxBorder? border;
  final BorderRadius? borderRadius;

  const SingleText(this.text,
      {Key? key,
        this.textStyle,
        this.height,
        this.padding,
        this.wide,
        this.alignment,
        this.margin,
        this.bgColor,
        this.border,
        this.borderRadius,this.maxLines})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: wide,
      height: height,
      padding: padding,
      margin: margin,
      alignment: alignment,
      constraints: BoxConstraints(minWidth: 10, maxWidth: double.infinity),
      decoration: BoxDecoration(
          color: bgColor, border: border, borderRadius: borderRadius),
      child: Text(
        text,
        style: textStyle,
        maxLines: maxLines,
      ),
    );
  }
}

///左标题,右内容的 text组件
class TitleContentText extends StatelessWidget {
  final String? title;
  final String? content;
  final TextStyle? titleStyle;
  final TextStyle? contentStyle;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final MainAxisAlignment? mainAxisAlignment;
  final CrossAxisAlignment? crossAxisAlignment;
  final Border? border;
  final Color? bgColor;
  final AlignmentGeometry? alignment;
  final double? height;
  final int? titleMaxLines;
  final BorderRadiusGeometry? borderRadius;

  const TitleContentText({Key? key,
    this.title,
    this.content,
    this.titleStyle,
    this.contentStyle,
    this.margin,
    this.padding,
    this.mainAxisAlignment,
    this.crossAxisAlignment,
    this.border,
    this.bgColor,
    this.borderRadius,
    this.alignment, this.height, this.titleMaxLines})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      padding: padding,
      height: height,
      decoration: BoxDecoration(border: border,
          color: bgColor ?? Colors.white,
          borderRadius: borderRadius),
      child: Row(
        mainAxisAlignment: mainAxisAlignment ?? MainAxisAlignment.start,
        crossAxisAlignment: crossAxisAlignment ?? CrossAxisAlignment.center,
        children: [
          Container(
            child: Text(
              title ?? "",
              maxLines: titleMaxLines ?? 1,
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              style: titleStyle ??
                  TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ),
          Expanded(
              child: Container(
                alignment: alignment,
                margin: const EdgeInsets.only(left: 8),
                child: Text(
                  content ?? "",
                  style: contentStyle,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ))
        ],
      ),
    );
  }
}

///左标题,右内容的 text组件,左右两边带有icon
class TitleContentIcon extends StatelessWidget {
  final String? title;
  final String? content;
  final TextStyle? titleStyle;
  final TextStyle? contentStyle;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? contentPadding;
  final MainAxisAlignment? mainAxisAlignment;
  final CrossAxisAlignment? crossAxisAlignment;
  final Border? border;
  final Widget? titleIcon;
  final Widget? contentIcon;
  final TextAlign? contentAlign;
  final double? wide;
  final double? height;
  final VoidCallback? onClick;
  final Color? backColor;
  final AlignmentGeometry? alignment;
  final int? maxLines;
  final BorderRadiusGeometry? borderRadius;
  ///是否可以选中复制
  final bool? selectable;

  const TitleContentIcon({Key? key,
    this.title,
    this.content,
    this.titleStyle,
    this.contentStyle,
    this.margin,
    this.padding,
    this.mainAxisAlignment,
    this.crossAxisAlignment,
    this.border,
    this.titleIcon,
    this.contentIcon,
    this.contentAlign,
    this.onClick,
    this.maxLines,
    this.height,
    this.backColor,
    this.wide,
    this.contentPadding,
    this.borderRadius,
    this.selectable,
    this.alignment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TitleWidgetIcon(
        title: title,
        content: (selectable ?? false) ? SelectableText(
          content ?? "",
          maxLines: maxLines,
          textAlign: contentAlign,
          style: contentStyle,
        ) : Text(
          content ?? "",
          maxLines: maxLines,
          textAlign: contentAlign,
          softWrap: true,
          style: contentStyle,
          overflow: maxLines == 1 ? TextOverflow.ellipsis : null,
        ),
        titleStyle: titleStyle,
        margin: margin,
        padding: padding,
        alignment: alignment,
        mainAxisAlignment: mainAxisAlignment,
        crossAxisAlignment: crossAxisAlignment,
        border: border,
        titleIcon: titleIcon,
        contentIcon: contentIcon,
        contentAlign: contentAlign,
        onClick: onClick,
        height: height,
        backColor: backColor ?? Colors.white,
        contentPadding: contentPadding,
        borderRadius: borderRadius,
        wide: wide);
  }
}

///左标题,右侧为widget,左右两边带有icon
class TitleWidgetIcon extends StatelessWidget {
  final String? title;
  final Widget? content;
  final TextStyle? titleStyle;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? contentPadding;
  final MainAxisAlignment? mainAxisAlignment;
  final CrossAxisAlignment? crossAxisAlignment;
  final Border? border;
  final Widget? titleIcon;
  final Widget? contentIcon;
  final TextAlign? contentAlign;
  final double? wide;
  final double? height;
  final VoidCallback? onClick;
  final Color? backColor;
  final AlignmentGeometry? alignment;
  final BorderRadiusGeometry? borderRadius;

  const TitleWidgetIcon({Key? key,
    this.title,
    this.content,
    this.titleStyle,
    this.margin,
    this.padding,
    this.mainAxisAlignment,
    this.crossAxisAlignment,
    this.border,
    this.titleIcon,
    this.contentIcon,
    this.contentAlign,
    this.onClick,
    this.height,
    this.backColor,
    this.borderRadius,
    this.wide,
    this.contentPadding,
    this.alignment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      child: Container(
        width: wide,
        height: height,
        margin: margin,
        alignment: Alignment.center,
        padding: padding,
        decoration: BoxDecoration(
            border: border, color: backColor, borderRadius: borderRadius),
        child: Row(
          mainAxisAlignment: mainAxisAlignment ?? MainAxisAlignment.start,
          crossAxisAlignment: crossAxisAlignment ?? CrossAxisAlignment.center,
          children: [
            titleIcon ?? Container(),
            Text(
              title ?? "",
              style:
              titleStyle ?? TextStyle(color: Colors.black87, fontSize: 16),
            ),
            Expanded(
                child: Container(
                  alignment: alignment ?? Alignment.centerRight,
                  margin: EdgeInsets.only(right: 4, left: 8),
                  padding: contentPadding,
                  child: content,
                )),
            contentIcon ?? Container()
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SingleEdit extends StatelessWidget {
  final AlignmentGeometry? alignment;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final Color? color;
  final BorderRadiusGeometry? borderRadius;
  final BoxBorder? border;
  final TextEditingController? controller;
  final InputDecoration? decoration;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final int? maxLines;
  final int? minLines;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onSubmitted;
  final TextAlign? textAlign;
  final bool? obscureText;
  final TextStyle? style;
  final String? hint;
  final double? wide;
  final bool? autoFocus;
  final bool? enable;

  SingleEdit(
      {Key? key,
      this.alignment,
      this.color,
      this.borderRadius,
      this.border,
      this.controller,
      this.decoration,
      this.keyboardType,
      this.textInputAction,
      this.maxLines,
      this.minLines,
      this.padding,
      this.hint,
      this.margin,
      this.textAlign,
      this.onChanged,
      this.style,
      this.onSubmitted,
      this.obscureText,
      this.wide,
      this.autoFocus,this.enable})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      margin: margin,
      width: wide,
      padding: padding,
      decoration: BoxDecoration(
          color: color, borderRadius: borderRadius, border: border),
      child: TextField(
        textAlign: textAlign ?? TextAlign.start,
        controller: controller,
        decoration: decoration,
        enabled: enable??true,
        keyboardType: keyboardType,
        obscureText: obscureText ?? false,
        textInputAction: textInputAction,
        autofocus: autoFocus??false,
        maxLines: (obscureText ?? false) ? 1 : maxLines,
        minLines: minLines,
        style: style,
        onChanged: onChanged,
        onSubmitted: onSubmitted,
      ),
    );
  }
}

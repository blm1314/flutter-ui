import 'dart:developer';
import 'dart:ui';
import 'dart:io';

import 'package:flutter/material.dart';

///常用带有toolbar的页面
class ToolbarPage extends StatelessWidget {
  final String title;
  final Widget body;
  final List<Widget>? actions;
  final bool? centerTitle;
  final bool? resizeBottom;
  final double? height;
  final Color? bgColor;
  final List<Widget>? persistentFooterButtons;

  const ToolbarPage(this.title, this.body,
      {this.actions,
      this.centerTitle,
      this.resizeBottom,
      this.height,
      this.bgColor,
      this.persistentFooterButtons});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        toolbarHeight: height,
        centerTitle: centerTitle ?? true,
        elevation: 0,
        actions: actions,
      ),
      resizeToAvoidBottomInset: resizeBottom ?? false,
      backgroundColor: bgColor,
      persistentFooterButtons: persistentFooterButtons,
      body: body,
    );
  }
}

///带有刷新/toolbar的页面
class ToolbarRefreshPage extends StatelessWidget {
  final String title;
  final Widget child;
  final RefreshCallback refresh;
  final List<Widget>? actions;
  final bool? centerTitle;

  const ToolbarRefreshPage(
      {required this.title,
      required this.child,
      required this.refresh,
      this.centerTitle,
      this.actions});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        centerTitle: centerTitle,
        elevation: 0,
      ),
      body: RefreshIndicator(child: child, onRefresh: refresh),
    );
  }
}

///页面状态
enum EmotionStatus { LOADING, FINISH, EMPTY, ERROR }

///带有状态的页面
class EmotionWidget extends StatelessWidget {
  final Widget child;
  final EmotionStatus status;
  final VoidCallback? retry;
  final String? emptyNotify;

  EmotionWidget(this.child, this.status,
      {this.retry, this.emptyNotify, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      alignment: status == EmotionStatus.FINISH
          ? Alignment.topCenter
          : Alignment.center,
      child: status == EmotionStatus.LOADING
          ? CircularProgressIndicator()
          : status == EmotionStatus.EMPTY
              ? GestureDetector(
                  onTap: retry,
                  child: Text(
                    emptyNotify ?? "暂无数据",
                    style: TextStyle(color: Colors.black54, fontSize: 14),
                  ),
                )
              : status == EmotionStatus.ERROR
                  ? InkWell(
                      onTap: retry,
                      child: Text(
                        "加载失败,点击重试",
                        style: TextStyle(color: Colors.black54, fontSize: 14),
                      ),
                    )
                  : child,
    );
  }
}

///加载更多组件
class LoadMoreWidget extends StatelessWidget {
  final bool hasMore;
  final bool? reverse;
  final List<Widget> children;
  final Function()? loadMore;
  final Function()? retry;
  final ScrollPhysics? physics;
  final Color? bgColor;
  final String? emptyNotify;

  LoadMoreWidget(this.children,
      {this.hasMore = false,
      this.loadMore,
      this.reverse,
      this.physics,
      this.retry,
      this.bgColor,
        this.emptyNotify,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: bgColor,
      alignment: children.isEmpty ? Alignment.center : Alignment.topCenter,
      child: children.isEmpty
          ? GestureDetector(
              onTap: retry,
              child: Text(
                emptyNotify?? '暂无数据',
                style: TextStyle(color: Colors.black54, fontSize: 14),
              ),
            )
          : ListView.builder(
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              physics: physics ??
                  (Platform.isIOS
                      ? const BouncingScrollPhysics()
                      : const ClampingScrollPhysics()),
              itemBuilder: (context, index) {
                if (hasMore && index == children.length - 2) {
                  loadMore?.call();
                }
                return children[index];
              },
              itemCount: children.length,
            ),
    );
  }
}

import 'package:flutter/material.dart';

import '../datas/enums.dart';
import '../datas/upgrade_base.dart';

///@author:create by BZY
///Date: 2022/12/26 11:01
///Description: 升级组件

class UpgradeWidget extends StatelessWidget {
  final UpgradeBase info;
  final VoidCallback onConfirm;
  final VoidCallback onCancel;
  final VoidCallback skip;
  final int? progress;
  final DownloadStatus status;

  UpgradeWidget(this.info, this.progress, this.status, this.onConfirm,
      this.onCancel,this.skip, Key? key)
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(8)),
      child: Column(
        children: [
          title(),
          fileSize(),
          versionName(),
          versionCode(),
          intro(),
          progressBar(),
          operation()
        ],
      ),
    );
  }

  ///更新标题
  Widget title() {
    return Container(
      height: 45,
      width: double.infinity,
      decoration:
          BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey))),
      alignment: Alignment.center,
      child: Text(
        info.getTitle(),
        style: TextStyle(fontSize: 18, color: Colors.black87),
      ),
    );
  }

  ///更新版本名称
  Widget versionName() {
    return Container(
      margin: EdgeInsets.only(left: 12, top: 6),
      alignment: Alignment.centerLeft,
      width: double.infinity,
      child: Text(
        '版本:${info.getVersionName()}',
        style: TextStyle(fontSize: 12, color: Colors.grey),
      ),
    );
  }

  ///更新版本号
  Widget versionCode() {
    return Container(
      margin: EdgeInsets.only(left: 12, top: 6),
      alignment: Alignment.centerLeft,
      width: double.infinity,
      child: Text(
        '版本号:${info.getVersionCode()}',
        style: TextStyle(fontSize: 12, color: Colors.grey),
      ),
    );
  }

  ///更新包大小
  Widget fileSize() {
    return info.getFileSize() > 0
        ? Container(
            margin: EdgeInsets.only(left: 12, top: 6),
            alignment: Alignment.centerLeft,
            width: double.infinity,
            child: Text(
              '更新包大小:${info.getFileSize()}M',
              style: TextStyle(fontSize: 12, color: Colors.grey),
            ),
          )
        : Container();
  }

  ///更新说明
  Widget intro() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.fromLTRB(12, 8, 12, 12),
      child: Text(
        info.getInfo(),
        softWrap: true,
        style: TextStyle(fontSize: 14, color: Colors.black87),
      ),
    );
  }

  ///按钮操作组件
  Widget operation() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          border: Border(top: BorderSide(color: Colors.grey, width: 0.6))),
      child: info.getForce()
          ? confirmBtn()
          : Row(
              children: [
                cancelBtn(),

                Container(
                  height: 25,
                  width: 0.6,
                  decoration: BoxDecoration(color: Colors.grey),
                ), skipBtn(),Container(
                  height: 25,
                  width: 0.6,
                  decoration: BoxDecoration(color: Colors.grey),
                ),
                Expanded(child: confirmBtn())
              ],
            ),
    );
  }

  ///确定按钮
  Widget confirmBtn() {
    return TextButton(
      onPressed: onConfirm,
      child: Text(
          status == DownloadStatus.undefined
              ? '立即更新'
              : status == DownloadStatus.running
                  ? '下载中'
                  : status == DownloadStatus.pause
                      ? '暂停中'
                      : status == DownloadStatus.fail
                          ? '重试'
                          : status == DownloadStatus.done
                              ? '下载完成'
                              : '立即更新',
          style: TextStyle(
              fontSize: 16,
              color: status == DownloadStatus.undefined
                  ? Colors.blue
                  : status == DownloadStatus.running
                      ? Colors.grey
                      : status == DownloadStatus.pause
                          ? Colors.grey
                          : status == DownloadStatus.fail
                              ? Colors.red
                              : status == DownloadStatus.done
                                  ? Colors.green
                                  : Colors.blue)),
    );
  }

  ///取消按钮
  Widget cancelBtn() {
    return Expanded(
        child: TextButton(
            style: ButtonStyle(
                textStyle: MaterialStateProperty.all(
                    TextStyle(fontSize: 16, color: Colors.black38))),
            onPressed: onCancel,
            child: Text(
              '取消',
              style: TextStyle(fontSize: 16, color: Colors.black38),
            )));
  }

///跳过按钮
  Widget skipBtn() {
    return Expanded(
        child: TextButton(
            style: ButtonStyle(
                textStyle: MaterialStateProperty.all(
                    TextStyle(fontSize: 16, color: Colors.black38))),
            onPressed: skip,
            child: Text(
              '不再提醒',
              style: TextStyle(fontSize: 16, color: Colors.black87),
            )));
  }

 Widget progressBar() {
    return (progress??0)>0?Container(
      child: LinearProgressIndicator(value: (progress??0)/100*1.0,),
    ):Container();
 }
}
